.. SPDX-FileCopyrightText: 2016-2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
.. SPDX-License-Identifier: AGPL-3.0-only

##########################
Geometry functions library
##########################

This repository contains some geometry functions.

