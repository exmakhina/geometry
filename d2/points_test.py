# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging

from .points import *


logger = logging.getLogger()


def test_distance():
	d = distance(0,0,1,1)
	assert d == 2**0.5
