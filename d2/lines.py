# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t


def swapped(x0, y0, x1, y1):
	"""
	:return: same line, swapped endpoints
	"""
	return x1, y1, x0, y0


def same(ref_x0, ref_y0, ref_x1, ref_y1, mes_x0, mes_y0, mes_x1, mes_y1) \
 -> bool:
	"""
	:return: whether two lines are the same
	"""
	return NotImplementedError()


def closest_point(x0, y0, x1, y1, x2, y2) \
 -> t.Tuple[float,float]:
	"""
	:return: closest point to p2 on line p0p1
	"""


def distance_point(x0, y0, x1, y1, x2, y2) -> float:
	cx, cy = closest_point(sx, sy, ex, ey, px, py)
	return points_symbolic.distance(px, py, cx, cy)
