#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import typing as t
import logging
import itertools

import sympy

from . import points_symbolic
from . import lines_symbolic
from . import segments_symbolic


logger = logging.getLogger(__name__)


def collinear4_slack(x0, y0, x1, y1, x2, y2, x3, y3, tol=0) -> bool:
	"""
	:return: whether segments are collinear, given some slack.

	We consider here that any segment and points from any 3 points of
	the two segments must respect the distance point-segment being
	less than the tolerance.
	"""

	p0 = (x0, y0)
	p1 = (x1, y1)
	p2 = (x2, y2)
	p3 = (x3, y3)

	args = []

	done = set()

	for a, b, c in itertools.permutations([p0, p1, p2, p3], 3):
		cur = hash((hash((a,b)), c))
		if cur in done:
			continue
		args += [lines_symbolic.distance_point(*a, *b, *c) <= tol]
		done.add(cur)

	return sympy.And(*args)


def contains_point_slack(x0, y0, x1, y1, x2, y2, eps, can_degenerate=True):
	"""
	:return: Whether p2 is on p0p1, with slack at extremities
	"""

	args = []
	if can_degenerate:
		val = segments_symbolic.length(x0, y0, x2, y2) <= eps
		cond = segments_symbolic.length(x0, y0, x1, y1) == 0
		args += [(val, cond)]

		# If we can alreayd prove, let's not get errors
		if isinstance(cond, bool) and cond:
			return val

	args += [
	 (
	  segments_symbolic.contains_point(
	  *segments_symbolic.elongated(x0, y0, x1, y1, sl=eps, el=eps), x2, y2),
	  True,
	 ),
	]

	return sympy.Piecewise(*args)


def contains_point_distance(x0, y0, x1, y1, x2, y2, eps):
	"""
	Whether segment contains a point, with a distance tolerance
	(distance from point to segment).
	"""
	return segments_symbolic.distance_point(x0, y0, x1, y1, x2, y2) <= eps


def merged_1(x0, y0, x1, y1, x2, y2, x3, y3, eps=0):
	"""

	DANGER - Slow, plus buggy version in case of superset situation.

	Merge two segments if they are absolutely collinear
	and their union is possible, with a tolerance for
	a small hole between them.

	:return: False if not mergeable, merged segment otherwise
	"""

	p0_p1_p2 = contains_point_slack(x0, y0, x2, y2, x1, y1, eps)
	p0_p1_p3 = contains_point_slack(x0, y0, x3, y3, x1, y1, eps)
	p0_p2_p1 = contains_point_slack(x0, y0, x1, y1, x2, y2, eps)
	p0_p2_p3 = contains_point_slack(x0, y0, x3, y3, x2, y2, eps)
	p0_p3_p1 = contains_point_slack(x0, y0, x1, y1, x3, y3, eps)
	p0_p3_p2 = contains_point_slack(x0, y0, x2, y2, x3, y3, eps)
	p1_p0_p2 = contains_point_slack(x1, y1, x2, y2, x0, y0, eps)
	p1_p0_p3 = contains_point_slack(x1, y1, x3, y3, x0, y0, eps)
	p1_p2_p0 = p0_p2_p1
	p1_p2_p3 = contains_point_slack(x1, y1, x3, y3, x2, y2, eps)
	p1_p3_p0 = p0_p3_p1
	p1_p3_p2 = contains_point_slack(x1, y1, x2, y2, x3, y3, eps)
	p2_p0_p1 = p1_p0_p2
	p2_p0_p3 = contains_point_slack(x2, y2, x3, y3, x0, y0, eps)
	p2_p1_p0 = p0_p1_p2
	p2_p1_p3 = contains_point_slack(x2, y2, x3, y3, x1, y1, eps)
	p2_p3_p1 = p1_p3_p2
	p3_p0_p2 = p2_p0_p3
	p3_p1_p2 = p2_p1_p3
	p3_p2_p1 = p1_p2_p3

	p0_p1 = sympy.Or(
	 sympy.And(p0_p2_p3, p2_p3_p1),
	 sympy.And(p0_p3_p2, p3_p2_p1),
	) # OK
	p0_p3 = sympy.Or(
	 sympy.And(p0_p2_p1, p2_p1_p3),
	 #, sympy.And(p0_p1_p2, p1_p2_p3)
	)
	p0_p2 = sympy.Or(
	 #sympy.And(p0_p1_p3, p1_p3_p2),
	 sympy.And(p0_p3_p1, p3_p1_p2),
	)
	p1_p2 = sympy.Or(
		#sympy.And(p1_p0_p3, p0_p3_p2),
		sympy.And(p1_p3_p0, p3_p0_p2),
	)
	p1_p3 = sympy.Or(
		#sympy.And(p1_p0_p2, p0_p2_p3),
		sympy.And(p1_p2_p0, p2_p0_p3))
	p2_p3 = sympy.Or(
	 sympy.And(p2_p0_p1, p0_p1_p3),
	 sympy.And(p2_p1_p0, p1_p0_p3),
	) # OK

	return sympy.Piecewise(
	 # Interesting shortcut
	 ((x0, y0, x1, y1), segments_symbolic.same(x0, y0, x1, y1, x2, y2, x3, y3)),
	 *segments_symbolic._collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3),

	 #((x0, y0, x1, y1), segments_symbolic.superset(x0, y0, x1, y1, x2, y2, x3, y3)),
	 #((x2, y2, x3, y3), segments_symbolic.superset(x2, y2, x3, y3, x0, y0, x1, y1)),

	 ((x0, y0, x1, y1), segments_symbolic.superset(
	  *segments_symbolic.elongated(x0, y0, x1, y1, eps, eps),
	  x2, y2, x3, y3)),
	 ((x2, y2, x3, y3), segments_symbolic.superset(
	  *segments_symbolic.elongated(x2, y2, x3, y3, eps, eps),
	  x0, y0, x1, y1)),

	 #((x0, y0, x1, y1), p0_p1),
	 ((x0, y0, x3, y3), p0_p3),
	 ((x0, y0, x2, y2), p0_p2),
	 ((x1, y1, x2, y2), p1_p2),
	 ((x1, y1, x3, y3), p1_p3),
	 #((x2, y2, x3, y3), p2_p3),
	 (False, True), # 0-1 .. 2-3
	)

def merged_2(x0, y0, x1, y1, x2, y2, x3, y3, eps=0):
	"""
	Merge two segments if they are absolutely collinear
	and their union is possible, with a tolerance for
	a small hole between them.

	:return: False if not mergeable, merged segment otherwise

	"""

	eps = eps / 2

	args = [
	 ( # if not aligned we can forget about merging
	  False,
	  sympy.Not(sympy.And(
	   lines_symbolic.collinear3(x0, y0, x1, y1, x2, y2),
	   lines_symbolic.collinear3(x0, y0, x1, y1, x3, y3),
	  )),
	 ),
	 # Interesting shortcut
	 #((x0, y0, x1, y1), segments_symbolic.same(x0, y0, x1, y1, x2, y2, x3, y3)),
	 # May be faster also
	 #*(segments_symbolic._collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3)[1:]),
	 #((x0, y0, x1, y1), segments_symbolic.superset(x0, y0, x1, y1, x2, y2, x3, y3)),
	 #((x2, y2, x3, y3), segments_symbolic.superset(x2, y2, x3, y3, x0, y0, x1, y1)),
	]

	x0e, y0e, x1e, y1e = segments_symbolic.elongated(x0, y0, x1, y1, eps, eps)
	x2e, y2e, x3e, y3e = segments_symbolic.elongated(x2, y2, x3, y3, eps, eps)

	def has(x0, y0, x1, y1, x2, y2):
		return segments_symbolic.collinear_contains_point(x0, y0, x1, y1, x2, y2)

	"""
	Handle separated cases that merge when slack is added
	::

	     in:  ▄▄▄▄▄▄▄▄      ▀▀▀▀▀▀▀▀▀▀
	          p0      p1    p2      p3
	          p0      p1    p3      p2
	          p1      p0    p2      p3
	          p1      p0    p3      p2
	"""
	if 0:
		for ref_a, ref_b, sa, ea, sb, eb in (
		  ((x0, y0), (x3, y3), (x0e, y0e), (x1e, y1e), (x2e, y2e), (x3e, y3e)),
		  ((x0, y0), (x2, y2), (x0e, y0e), (x1e, y1e), (x3e, y3e), (x2e, y2e)),
		  ((x1, y1), (x3, y3), (x1e, y1e), (x0e, y0e), (x2e, y2e), (x3e, y3e)),
		  ((x1, y1), (x2, y2), (x1e, y1e), (x0e, y0e), (x3e, y3e), (x2e, y2e)),
		 ):
			args += [
			 (
			  (*ref_a, *ref_b),
			  sympy.And(
			   has(*sa, *ea, *sb),
			   has(*sb, *eb, *ea),
			  ),
			 ),
			]


	"""
	Handle superset case p0p1 ⊃ p2p3
	::

	     in:  ▀▀▀▀▀██████▀▀▀▀▀▀▀▀▀▀

	          p0   p2   p3   p1
	          p0   p3   p2   p1
	          p1   p2   p3   p0
	          p1   p3   p2   p0

	"""
	for ref_a, ref_b, sa, ea, sb, eb in (
	  ((x0, y0), (x1, y1), (x0e, y0e), (x1e, y1e), (x2e, y2e), (x3e, y3e)),
	  ((x0, y0), (x1, y1), (x0e, y0e), (x1e, y1e), (x3e, y3e), (x2e, y2e)),
	  ((x1, y1), (x0, y0), (x1e, y1e), (x0e, y0e), (x2e, y2e), (x3e, y3e)),
	  ((x1, y1), (x0, y0), (x1e, y1e), (x0e, y0e), (x3e, y3e), (x2e, y2e)),
	 ):
		args += [
		 (
		  (*ref_a, *ref_b),
		  sympy.And(
		   has(*sa, *ea, *sb),
		   has(*sa, *ea, *eb),
		  ),
		 ),
		]

	"""
	Handle superset case p2p3 ⊃ p1p2
	::

	     in:  ▀▀▀▀▀██████▀▀▀▀▀▀▀▀▀▀

	          p2   p0   p1   p3
	          p2   p1   p0   p3
	          p3   p0   p1   p2
	          p3   p1   p0   p2
	"""
	for ref_a, ref_b, sa, ea, sb, eb in (
	  ((x2, y2), (x3, y3), (x2e, y2e), (x3e, y3e), (x0e, y0e), (x1e, y1e)),
	  ((x2, y2), (x3, y3), (x2e, y2e), (x3e, y3e), (x1e, y1e), (x0e, y0e)),
	  ((x3, y3), (x2, y2), (x3e, y3e), (x2e, y2e), (x0e, y0e), (x1e, y1e)),
	  ((x3, y3), (x2, y2), (x3e, y3e), (x2e, y2e), (x1e, y1e), (x0e, y0e)),
	 ):
		args += [
		 (
		  (*ref_a, *ref_b),
		  sympy.And(
		   has(*sa, *ea, *sb),
		   has(*sa, *ea, *eb),
		  ),
		 ),
		]

	"""
	Handle overlapping cases
	::

	     in:  ▄▄▄▄▄▄▄▄██████▀▀▀▀▀▀▀▀▀▀
	Or touching:
	::

	         sa      sb ea     eb
	     in:  ▄▄▄▄▄▄▄▄▀▀▀▀▀▀▀▀▀▀

	          p0      p2  x  p1      p3
	          p0      p3  x  p1      p2
	          p1      p2  x  p0      p3
	          p1      p3  x  p0      p2
	"""
	for ref_a, ref_b, sa, ea, sb, eb in (
	  ((x0, y0), (x3, y3), (x0e, y0e), (x1e, y1e), (x2e, y2e), (x3e, y3e)),
	  ((x0, y0), (x2, y2), (x0e, y0e), (x1e, y1e), (x3e, y3e), (x2e, y2e)),
	  ((x1, y1), (x3, y3), (x1e, y1e), (x0e, y0e), (x2e, y2e), (x3e, y3e)),
	  ((x1, y1), (x2, y2), (x1e, y1e), (x0e, y0e), (x3e, y3e), (x2e, y2e)),
	 ):
		args += [
		 (
		  (*ref_a, *ref_b),
		  sympy.And(
		   has(*sa, *ea, *sb),
		   has(*sb, *eb, *ea),
		  ),
		 ),
		]

	args += [
	 (False, True), # disjoint
	]

	return sympy.Piecewise(*args)

merged = merged_2


def intersection(x0, y0, x1, y1, x2, y2, x3, y3, eps=0) \
 -> t.Optional[t.Tuple[t.Any,t.Any]]:
	"""
	:return: intersection of 2 segments p0p1 and p2p3,
	with a tolerance for elongation.
	"""
	x0,y0, x1,y1 = segments_symbolic.elongated(x0,y0, x1,y1, eps, eps)
	x2,y2, x3,y3 = segments_symbolic.elongated(x2,y2, x3,y3, eps, eps)
	return segments_symbolic.intersection(x0,y0, x1,y1, x2,y2, x3,y3)
