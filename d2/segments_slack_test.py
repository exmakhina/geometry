#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import logging

from . import points
from . import segments
from .segments_slack import *

def test_intersection():
	res = intersection(0,0, 2,2, 2,0, 0,2, eps=1.01)
	assert res, f"Wanted to find intersection, got {res}"
	assert points.same(*res, *(1, 1), eps=1e-5), f"Wanted 1, 1 got {res}"

	res = intersection(2,2,0,0, 2,0,2,1, eps=1.01)
	assert res, f"Wanted to find intersection, got {res}"
	assert points.same(*res, *(2, 2), eps=1e-5), f"Wanted 2, 2 got {res}"

	res = intersection(2,2,0,0, 3,3,4,4, eps=1.01)
	assert not res, f"Wanted no intersection, got {res}"
	#assert points.same(*res, *(2, 2), eps=1e-5), f"Wanted 2, 2 got {res}"

	res = intersection(2,2,0,0, 3,3,4,5, eps=1.01)
	assert not res, f"Wanted no intersection, got {res}"

def test_merged():
	res = merged(0, 0,  1, 1,  11, 11, 2, 2, 2**0.5+0.1)
	assert segments.same(0,0, 11,11, *res)
