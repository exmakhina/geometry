#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import sympy

from . import lines_symbolic


def collinear3_slack(x0, y0, x1, y1, x2, y2, eps=0):
	"""
	:return: Whether p2 is on p0p1
	"""
	return lines_symbolic.distance_point(x0, y0, x1, y1, x2, y2) <= eps
