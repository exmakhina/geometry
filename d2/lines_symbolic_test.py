# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import itertools

from . import lines

from .lines_symbolic import *
from . import points_symbolic



logger = logging.getLogger(__name__)


def v(x, y):
	return sympy.Matrix([x, y])


def test_same_proof():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)
	r3 = sympy.Symbol("r3", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		p0 = v(x0, y0)

		vd = v(dx*ox, dy*oy)

		p1 = p0 + r0*vd

		p2 = p1 + r1*vd

		p3 = p2 + r2*vd

		for a, b, c, d in itertools.permutations([p0, p1, p2, p3]):
			res = same(*a, *b, *c, *d)
			res = res.simplify()
			assert res == True

		"""
		Now to prove that lines are not the same, we shift p3 away
		"""

		angle = sympy.atan2(1, r3)
		mr = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		])

		p3 = p2 + r2*mr*vd

		for a, b, c, d in itertools.permutations([p0, p1, p2, p3]):
			res = same(*a, *b, *c, *d)
			res = res.simplify()
			logger.debug("res (should ideally be False): %s", res)
			# 
			if isinstance(res, sympy.Eq):
				# https://github.com/sympy/sympy/issues/26378
				# https://github.com/sympy/sympy/issues/26380
				res = sympy.Eq((res.lhs - res.rhs).expand().simplify(), 0)
			assert res == False


def test_collinear3_cases():
	for (dx,dy) in ((0,1),(1,0),(0,-1),(-1,0), (1,1), (1,-1), (-1,1), (-1,-1)):
		for (x0, y0) in ((0,0),):
			for mul in (1,2,-1,0):
				x1 = x0 + dx
				y1 = y0 + dy
				x2 = x0 + dx*mul
				y2 = y0 + dy*mul
				res = collinear3(x0, y0, x1, y1, x2, y2)
				assert res

				# Put a point in a direction we aren't implementing
				x2 += 11
				y2 += 13
				res = collinear3(x0, y0, x1, y1, x2, y2)
				if res:
					logger.error("Shouldn't be collinear: %s", (x0, y0, x1, y1, x2, y2))
				assert not res


def test_collinear3_proof():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):

		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x0 + r1*dx*ox
		y2 = y0 + r1*dy*oy

		p0 = [x0, y0]
		p1 = [x1, y1]
		p2 = [x2, y2]

		logger.debug("%s %s %s", p0, p1, p2)

		res = collinear3(x0, y0, x1, y1, x2, y2)
		assert res

		res = collinear3(x0, y0, x1, y1, x2+1, y2).simplify()
		# The result is an And of simultaneously impossible conditions
		# but simplify can't see it
		logger.debug("res: %s", res)
		sol = sympy.solve(res.args)
		logger.debug("res: %s", sol)
		assert not sol

		res = collinear3(x0, y0, x1, y1, x2, y2+1).simplify()
		logger.debug("res: %s", res)
		sol = sympy.solve(res.args)
		logger.debug("res: %s", sol)
		assert not sol


def v(x, y):
	return sympy.Matrix([x, y])

def test_closest_point_on_line():
	res = closest_point(0,0, 2,0, 1,1)
	assert points_symbolic.same(*res, *(1, 0))

def test_closest_point_on_line_proof():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x1 + r1*dx*ox
		y2 = y1 + r1*dy*oy

		v2 = r2 * v(dx*ox, dy*oy)
		angle = sympy.pi/2
		v2 = sympy.Matrix([[sympy.cos(angle), sympy.sin(angle)],[-sympy.sin(angle), sympy.cos(angle)]]) * v2
		p3 = v(x1, y1) + v2
		x3, y3 = p3

		p0 = v(x0, y0)
		p1 = v(x1, y1)
		p2 = v(x2, y2)
		p3 = v(x3, y3)

		res = sympy.Matrix(closest_point(*p0, *p2, *p3))
		diff = res - p1
		diff.simplify()
		assert diff == v(0, 0)
