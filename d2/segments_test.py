#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import logging
import typing as t

from .segments import *
from . import points


logger = logging.getLogger()


def test_segment_closest():
	sx, sy = 0, 0
	ex, ey = 10, 10
	px, py = 5,0
	cx, cy = closest_point_on_segment(sx, sy, ex, ey, px, py)
	assert points.same(cx, cy, 2.5, 2.5)

	sx, sy = 0, 0
	ex, ey = 10, 10
	px, py = 11, 11
	cx, cy = closest_point_on_segment(sx, sy, ex, ey, px, py)
	assert points.same(cx, cy, 10, 10)


def test_collapse_adjacent():
	sx1, sy1, ex1, ey1 = 0,0,1,1
	sx2, sy2, ex2, ey2 = 1,1,2,2
	m = collapse_adjacent(sx1, sy1, ex1, ey1, sx2, sy2, ex2, ey2)
	assert m == (0,0,2,2)

	sx1, sy1, ex1, ey1 = 0,0,1,1
	sx2, sy2, ex2, ey2 = 1,1.1,2,2
	m = collapse_adjacent(sx1, sy1, ex1, ey1, sx2, sy2, ex2, ey2)
	assert m is None, "not aligned, not touching"
