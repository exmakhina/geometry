#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import logging

from exmakhina.sympy_extras.manip import relational as manip_relational
from exmakhina.sympy_extras.manip.piecewise import apply as piecewise_apply
from exmakhina.sympy_extras.manip import piecewise as manip_piecewise
from exmakhina.sympy_extras.manip import base as manip_base
from exmakhina.sympy_extras.manip.base import (transformed, multipass)

from . import lines
from . import lines_symbolic
from . import points_symbolic
from . import segments_symbolic

from .segments_symbolic_slack import *


logger = logging.getLogger(__name__)

def v(x, y):
	return sympy.Matrix([x, y])

def test_collinear4_slack():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)
	r3 = sympy.Symbol("r3", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		p0 = v(x0, y0)

		vd = v(dx*ox, dy*oy)

		p1 = p0 + r0*vd

		p2 = p1 + r1*vd

		p3 = p2 + r2*vd

		res = collinear4_slack(*p0, *p1, *p2, *p3)
		logger.debug("res: %s", res)
		res = res.simplify()
		logger.debug("res: %s", res)
		assert res == True

		angle = sympy.atan2(r3, 1)
		mr = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		])

		p3 = p2 + r2*mr*vd

		res = collinear4_slack(*p0, *p1, *p2, *p3)
		logger.debug("res: %s", res)
		res = res.simplify()
		logger.debug("res: %s", res)
		assert res == False

		# TODO add tolerance test, but by construction we're OK


def test_merged_slack_cases():
	res = merged(0, 0,  1, 1,  11, 11, 2, 2, sympy.sqrt(2))
	assert segments_symbolic.same(0,0, 11,11, *res)

	a = 0, 0, 0, 4
	b = 0, 1, 0, 4
	M = merged(*b, *a, 0)
	logger.info("Merged: %s", M)
	assert M == (0, 0, 0, 4)


def test_merged_slack_proof():
	"""
	"""

	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	a = sympy.Symbol("a", positive=True)
	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)
	r3 = sympy.Symbol("r3", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for quadrant in (0, sympy.pi/2, sympy.pi, -sympy.pi/2):
		logger.info("Quadrant %s", quadrant)
		angle = quadrant + sympy.atan2(a, 1)
		mr = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		])
		vd = mr * v(1,0)

		p0 = v(x0, y0)

		swapped = lines.swapped
		same = segments_symbolic.same
		runme = True

		if runme:
			logger.info("Case where the two arguments are the same")
			p1 = p0 + r1*vd
			slack = sympy.Symbol("slack", positive=True)
			res = merged(*p0, *p1, *p0, *p1, slack)
			res = res.simplify()
			logger.debug("res: %s", res)
			assert res, f"oops"
			assert same(*p0, *p1, *res), f"oops"

		if runme:
			logger.info("Case where the two arguments are the same")
			p1 = p0 + r1*vd
			slack = sympy.Symbol("slack", positive=True)
			res = merged(*p0, *p1, *p1, *p0, slack)
			res = res.simplify()
			logger.debug("res: %s", res)
			assert res, f"oops"
			assert same(*p0, *p1, *res) or same(*p1, *p0, *res), f"oops"

		if runme:
			logger.info("Case where one segment is a superset of the other")
			slack = 0
			p1 = p0 + r0*vd
			p2 = p1 + r1*vd
			p3 = p2 + r2*vd
			res = merged(*p1, *p2, *p0, *p3, slack)
			res = res.simplify()
			logger.debug("res: %s", res)
			assert res, f"oops"
			assert same(*p0, *p3, *res), f"oops"

		if runme:
			logger.info("Case where we're touching before extension")
			p1 = p0 + r0*vd
			p2 = p1 + r1*vd
			slack = 0
			res = merged(*p0, *p1, *p1, *p2, slack)
			logger.debug("res: %s", res)
			assert res, f"oops, we should reach"
			res = transformed(res, lambda x: piecewise_apply(x, cond_func=sympy.expand))
			logger.debug("res: %s", res)
			assert same(*p0, *p2, *res), f"oops"

		if runme:
			logger.info("Case where we're touching after extension")
			p1 = p0 + r0*vd
			p2 = p1 + r1*vd
			p3 = p2 + r2*vd
			slack = r1
			logger.debug("Slack: %s", slack)

			res = merged(*p0, *p1, *p2, *p3, slack)
			logger.debug("res: %s", res)

			res = manip_base.multipass(res,
			 lambda x: transformed(x, manip_relational.normalize),
			 manip_base.simplify_radicals_by_matching_bases,
			 lambda x: transformed(x, lambda y: manip_piecewise.apply(y, cond_func=sympy.simplify)),
			)
			assert res, f"oops, we should reach"
			assert same(*p0, *p3, *res), f"oops"

		if runme:
			logger.info("Case where one segment is a superset of the other ; any slack added should not hurt")
			p1 = p0 + r0*vd
			p2 = p1 + r1*vd
			p3 = p2 + r2*vd
			slack = r1+sympy.Symbol("slack", positive=True)
			res = merged(*p1, *p2, *p0, *p3, slack)

			res = manip_base.multipass(res,
			 lambda x: transformed(x, manip_relational.normalize),
			 lambda x: transformed(x, lambda y: manip_piecewise.apply(y, cond_func=sympy.simplify)),
			 lambda x: transformed(x, lambda y: manip_relational.simplify_relational_by_positive_factor(y)),
			 manip_base.simplify_radicals_by_matching_bases,
			)
			logger.debug("res: %s", res)
			assert same(*p0, *p3, *res)

		if runme:
			logger.info("Case where we're not reachable")
			slack = r1/2
			p1 = p0 + r0*vd
			p2 = p1 + r1*vd
			p3 = p2 + r2*vd

			res = merged(*p0, *p1, *p2, *p3, slack)
			logger.debug("res: %s", res)
			res = multipass(res,
			 lambda x: transformed(x, manip_relational.normalize),
			 lambda x: transformed(x, lambda y: manip_piecewise.apply(y, cond_func=sympy.simplify)),
			)
			logger.debug("res: %s", res)
			assert not res, f"Gap should be unbridgeable but got {res}"

		if runme:
			logger.info("Case where we're overlapping after extension")
			"""
			p0 === r0 = r3 === p1   r1    p2 === r3 = r0 === p3
			"""
			slack = r1+r3
			p1 = p0 + (r0+r3)*vd
			p2 = p1 + r1*vd
			p3 = p2 + (r2+r3)*vd
			logger.debug("Slack: %s", slack)
			#logger.info("p1p2 = %s", distance(*p1, *p2))

			res = merged(*p0, *p1, *p2, *p3, slack)
			logger.debug("res: %s", res)
			res = multipass(res,
			 lambda x: transformed(x, manip_relational.normalize),
			 lambda x: transformed(x, lambda y: manip_piecewise.apply(y, cond_func=sympy.simplify)),
			)
			logger.debug("res: %s", res)
			assert res, f"oops, we should reach"
			assert same(*p0, *p3, *res).simplify(), f"oops"

def test_intersection_cases():
	res = intersection(0,0, 2,2, 2,0, 2,1, eps=1)
	res = res.simplify()
	assert res, f"Wanted to find intersection, got {res}"
	assert points_symbolic.same(*res, *(2, 2)), f"Wanted 2, 2 got {res}"


	res = intersection(0,2, 2,2, 0,0, 1,1, eps=2)
	res = res.simplify()
	assert res, f"Wanted to find intersection, got {res}"
	assert points_symbolic.same(*res, *(2, 2)), f"Wanted 2, 2 got {res}"

	res = intersection(0,0, 1,0, 1,2, 1,1)
	res = res.simplify()
	assert not res, f"Wanted no intersection got {res}"
