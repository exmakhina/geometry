# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging

from .lines import *


logger = logging.getLogger(__name__)


def notest_same():
	raise NotImplementedError()
