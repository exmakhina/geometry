# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging

import sympy

from . import points_symbolic
from . import lines_symbolic


logger = logging.getLogger(__name__)


def same(ref_x0, ref_y0, ref_x1, ref_y1, mes_x0, mes_y0, mes_x1, mes_y1) \
 -> bool:
	"""
	:return: whether two segments are the same
	"""
	return sympy.Or(
	 sympy.And(
	  sympy.Eq(ref_x0 - mes_x0, 0),
	  sympy.Eq(ref_y0 - mes_y0, 0),
	  sympy.Eq(ref_x1 - mes_x1, 0),
	  sympy.Eq(ref_y1 - mes_y1, 0),
	 ),
	 sympy.And(
	  sympy.Eq(ref_x0 - mes_x1, 0),
	  sympy.Eq(ref_y0 - mes_y1, 0),
	  sympy.Eq(ref_x1 - mes_x0, 0),
	  sympy.Eq(ref_y1 - mes_y0, 0),
	 ),
	)


def length(x0, y0, x1, y1) -> t.Any:
	"""
	:return: segment length
	"""
	return points_symbolic.distance(x0, y0, x1, y1)


def collinear4(x0, y0, x1, y1, x2, y2, x3, y3, eps=0) -> bool:
	"""
	:return: whether segments are collinear
	"""

	s01dx = x1 - x0
	s01dy = y1 - y0

	s02dx = x2 - x0
	s02dy = y2 - y0

	zero_if_p2_aligned = s02dx * s01dy - s02dy * s01dx

	s03dx = x3 - x0
	s03dy = y3 - y0

	zero_if_p3_aligned = s03dx * s01dy - s03dy * s01dx

	return sympy.And(
	 sympy.Eq(zero_if_p2_aligned, 0),
	 sympy.Eq(zero_if_p3_aligned, 0),
	)


def elongated(sx, sy, ex, ey, sl=0, el=0) \
 -> t.Tuple[t.Any, t.Any, t.Any, t.Any]:
	"""
	Elongate a (non-null) segment at its starting and/or ending points.

	:param sx, sy: Coordinates of the starting point of the segment.
	:param ex, ey: Coordinates of the ending point of the segment.
	:param sl: Length to elongate towards the starting point.
	:param el: Length to elongate towards the ending point.

	:return: A tuple containing the coordinates of the new elongated segment (nsx, nsy, nex, ney).
	"""

	# Vector from start to end
	dx, dy = ex - sx, ey - sy

	# Normalize the vector
	length = sympy.sqrt(dx**2 + dy**2)
	dx /= length
	dy /= length

	# Calculate new start and end points
	nsx, nsy = sx - sl * dx, sy - sl * dy
	nex, ney = ex + el * dx, ey + el * dy

	return nsx, nsy, nex, ney


def contains_point_using_distance(x0, y0, x1, y1, x2, y2) -> bool:
	"""
	:return: Whether p2 is on p0p1
	"""

	return sympy.Eq(distance_point_segment(x0, y0, x1, y1, x2, y2), 0)



def collinear_contains_point(x0, y0, x1, y1, x2, y2) -> bool:
	s01dx = x1 - x0
	s01dy = y1 - y0
	s02dx = x2 - x0
	s02dy = y2 - y0

	return sympy.Or(
	 sympy.And(
	  sympy.LessThan(0, s02dx),
	  sympy.LessThan(0, s01dx),
	  sympy.LessThan(s02dx, s01dx),
	  sympy.LessThan(0, s02dy),
	  sympy.LessThan(0, s01dy),
	  sympy.LessThan(s02dy, s01dy),
	 ),
	 sympy.And(
	  sympy.GreaterThan(0, s02dx),
	  sympy.GreaterThan(0, s01dx),
	  sympy.GreaterThan(s02dx, s01dx),
	  sympy.GreaterThan(0, s02dy),
	  sympy.GreaterThan(0, s01dy),
	  sympy.GreaterThan(s02dy, s01dy),
	 ),
	 sympy.And(
	  sympy.GreaterThan(0, s02dx),
	  sympy.GreaterThan(0, s01dx),
	  sympy.GreaterThan(s02dx, s01dx),
	  sympy.LessThan(0, s02dy),
	  sympy.LessThan(0, s01dy),
	  sympy.LessThan(s02dy, s01dy),
	 ),
	 sympy.And(
	  sympy.LessThan(0, s02dx),
	  sympy.LessThan(0, s01dx),
	  sympy.LessThan(s02dx, s01dx),
	  sympy.GreaterThan(0, s02dy),
	  sympy.GreaterThan(0, s01dy),
	  sympy.GreaterThan(s02dy, s01dy),
	 ),
	)

def contains_point_using_collinear(x0, y0, x1, y1, x2, y2) -> bool:
	"""
	:return: Whether p2 is on p0p1
	"""

	for arg in (x0, y0, x1, y1, x2, y2):
		if isinstance(arg, sympy.core.numbers.NaN):
			raise ValueError(arg)

	return sympy.And(
	 lines_symbolic.collinear3(x0, y0, x1, y1, x2, y2),
	 collinear_contains_point(x0, y0, x1, y1, x2, y2),
	)


def contains_point(x0, y0, x1, y1, x2, y2) -> bool:
	return contains_point_using_collinear(x0, y0, x1, y1, x2, y2)


def superset(x0, y0, x1, y1, x2, y2, x3, y3) -> bool:
	"""
	:return: whether p0p1 is a superset of p2p3
	"""
	return sympy.And(
	 contains_point(x0, y0, x1, y1, x2, y2),
	 contains_point(x0, y0, x1, y1, x3, y3),
	)


def closest_point(x0, y0, x1, y1, x2, y2):
	"""
	:return: closest point to p2 that lays on segment p0p1
	"""

	# Vector SE
	sex, sey = x1 - x0, y1 - y0

	# Vector SP
	spx, spy = x2 - x0, y2 - y0

	d2 = (sex * sex + sey * sey)

	# Compute the projection t of SP onto SE
	t = (spx * sex + spy * sey) / d2

	# Clamp the projection parameter between [0, 1] to find the closest point on the line segment SE
	t = sympy.Max(0, sympy.Min(1, t))

	# Compute the closest point C
	cx = x0 + t * sex
	cy = y0 + t * sey

	return cx, cy


def distance_point_segment(sx, sy, ex, ey, px, py):
	cx, cy = closest_point(sx, sy, ex, ey, px, py)
	return points_symbolic.distance(px, py, cx, cy)

distance_point = distance_point_segment

def intersection(x0, y0, x1, y1, x2, y2, x3, y3) \
 -> t.Optional[t.Tuple[t.Any,t.Any]]:
	"""
	:return: intersection of 2 segments p0p1 and p2p3, if it exists, or False
	"""

	dx1, dy1 = x1 - x0, y1 - y0
	dx2, dy2 = x3 - x2, y3 - y2

	det = dx1 * dy2 - dy1 * dx2

	delta_x, delta_y = x2 - x0, y2 - y0

	_1_det = sympy.Pow(det, -1, evaluate=False)

	t1 = -dx2 * delta_y + dy2 * delta_x
	t2 = -dx1 * delta_y + dy1 * delta_x

	xi1 = det * x0 + t1 * dx1
	yi1 = det * y0 + t1 * dy1
	xi2 = det * x2 + t2 * dx2
	yi2 = det * y2 + t2 * dy2

	return sympy.Piecewise(
	 (False, sympy.Eq(det, 0)),
	 (
	  (xi1 * _1_det, yi1 * _1_det),
	  sympy.Or(
	   sympy.And(det > 0, 0 <= t1, t1 <= det, 0 <= t2, t2 <= det),
	   sympy.And(det < 0, 0 >= t1, t1 >= det, 0 >= t2, t2 >= det),
	  )
	 ),
	 (False, True),
	)


def _collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3):
	coll = collinear4(x0, y0, x1, y1, x2, y2, x3, y3)

	parts = [
	 (
	  False,
	  sympy.Not(coll),
	 )
	]
	for extrem_a, middle_a, middle_b, extrem_b in (
	  #                                           p0 & p1 - impossible
	  ((x1, y1), (x0, y0), (x2, y2), (x3, y3)), # p0 & p2
	  ((x1, y1), (x0, y0), (x3, y3), (x2, y2)), # p0 & p3
	  # p1 & p0 done
	  ((x0, y0), (x1, y1), (x2, y2), (x3, y3)), # p1 & p2
	  ((x0, y0), (x1, y1), (x3, y3), (x2, y2)), # p1 & p3
	  # p2 & p0 done, p2 & p1 done, p2 & p3 impossible
	  # p3 & p0 done, p3 & p1 done, p3 & p2 impossible
	 ):
		parts += [
		 (
		  tuple([*extrem_a, *extrem_b]),
		  points_symbolic.same(*middle_a, *middle_b),
		 ),
		]
	return parts

def collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3) \
 -> t.Optional[t.Tuple[t.Any,t.Any,t.Any,t.Any]]:
	"""
	Merge two segments if they are collinear and share exactly one end point.

	:return: The merged segment as a tuple (sx, sy, ex, ey)
	 or None if segments are not collinear or chained.
	"""

	parts = _collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3)

	return sympy.Piecewise(*parts,
	 (False, True),
	)


def merged(x0, y0, x1, y1, x2, y2, x3, y3) \
 -> t.Optional[t.Tuple[t.Any,t.Any,t.Any,t.Any]]:
	"""
	Merge two segments if their union would be possible

	::

	   example good (overlap):
	     in:  ▄▄▄▄▄▄▄▄██████▀▀▀▀▀▀▀▀▀▀
	     out: ████████████████████████

	   example good (touching):
	     in:  ▄▄▄▄▄▄▄▄▀▀▀▀▀▀▀▀▀▀
	     out: ██████████████████

	   example bad (disjoint):
	     in:  ▄▄▄▄▄▄▄▄      ▀▀▀▀▀▀▀▀▀▀

	If touching, the implementation relies on collapse_adjacent guts.

	TODO make more elegant by reusing code
	"""

	p0_p1_p2 = contains_point(x0, y0, x2, y2, x1, y1)
	p0_p1_p3 = contains_point(x0, y0, x3, y3, x1, y1)
	p0_p2_p1 = contains_point(x0, y0, x1, y1, x2, y2)
	p0_p2_p3 = contains_point(x0, y0, x3, y3, x2, y2)
	p0_p3_p1 = contains_point(x0, y0, x1, y1, x3, y3)
	p0_p3_p2 = contains_point(x0, y0, x2, y2, x3, y3)
	p1_p0_p2 = contains_point(x1, y1, x2, y2, x0, y0)
	p1_p0_p3 = contains_point(x1, y1, x3, y3, x0, y0)
	p1_p2_p0 = p0_p2_p1
	p1_p2_p3 = contains_point(x1, y1, x3, y3, x2, y2)
	p1_p3_p0 = p0_p3_p1
	p1_p3_p2 = contains_point(x1, y1, x2, y2, x3, y3)
	p2_p0_p1 = p1_p0_p2
	p2_p0_p3 = contains_point(x2, y2, x3, y3, x0, y0)
	p2_p1_p0 = p0_p1_p2
	p2_p1_p3 = contains_point(x2, y2, x3, y3, x1, y1)
	p2_p3_p1 = p1_p3_p2
	p3_p0_p2 = p2_p0_p3
	p3_p1_p2 = p2_p1_p3
	p3_p2_p1 = p1_p2_p3

	p0_p1 = sympy.Or(
		sympy.And(p0_p2_p3, p2_p3_p1),
		sympy.And(p0_p3_p2, p3_p2_p1),
	) # OK
	p0_p3 = sympy.Or(
	 sympy.And(p0_p2_p1, p2_p1_p3),
	 #, sympy.And(p0_p1_p2, p1_p2_p3)
	)
	p0_p2 = sympy.Or(
	 #sympy.And(p0_p1_p3, p1_p3_p2),
	 sympy.And(p0_p3_p1, p3_p1_p2),
	)
	p1_p2 = sympy.Or(
		#sympy.And(p1_p0_p3, p0_p3_p2),
		sympy.And(p1_p3_p0, p3_p0_p2),
	)
	p1_p3 = sympy.Or(
		#sympy.And(p1_p0_p2, p0_p2_p3),
		sympy.And(p1_p2_p0, p2_p0_p3))
	p2_p3 = sympy.Or(
	 sympy.And(p2_p0_p1, p0_p1_p3),
	 sympy.And(p2_p1_p0, p1_p0_p3),
	) # OK

	return sympy.Piecewise(
	 # shortcut
	 ((x0, y0, x1, y1), same(x0, y0, x1, y1, x2, y2, x3, y3)),
	 ((x0, y0, x1, y1), superset(x0, y0, x1, y1, x2, y2, x3, y3)),
	 ((x2, y2, x3, y3), superset(x2, y2, x3, y3, x0, y0, x1, y1)),
	 *_collapse_adjacent(x0, y0, x1, y1, x2, y2, x3, y3),
	 #((x0, y0, x1, y1), p0_p1),
	 ((x0, y0, x3, y3), p0_p3),
	 ((x0, y0, x2, y2), p0_p2),
	 ((x1, y1, x2, y2), p1_p2),
	 ((x1, y1, x3, y3), p1_p3),
	 #((x2, y2, x3, y3), p2_p3),
	 (False, True),
	)
