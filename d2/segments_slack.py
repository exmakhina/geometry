#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import typing as t
import logging
import math


logger = logging.getLogger(__name__)


def intersection(x0, y0, x1, y1, x2, y2, x3, y3, eps=1e-9) \
 -> t.Optional[t.Tuple[float,float]]:
	dx01 = -x0 + x1
	dy01 = -y0 + y1
	dx02 = -x0 + x2
	dy02 = -y0 + y2
	dx23 = -x2 + x3
	dy23 = -y2 + y3
	sql01 = dx01**2 + dy01**2
	sql23 = dx23**2 + dy23**2
	tmp_2 = 2*eps
	tmp_4 = 1/math.sqrt(sql23)
	tmp_5 = tmp_2*tmp_4 + 1
	tmp_0 = dx01*dy23 - dx23*dy01
	tmp_1 = 1/math.sqrt(sql01)
	tmp_3 = tmp_1*tmp_2 + 1
	tmp_6 = tmp_0*tmp_3*tmp_5
	#logger.debug("tmp6=%s", tmp_6)
	if tmp_6 == 0:
		return
	tmp_11 = eps*tmp_4
	tmp_8 = eps*tmp_1
	tmp_10 = dy01*tmp_8
	tmp_12 = dy02 - dy23*tmp_11 + tmp_10
	tmp_9 = dx01*tmp_8
	tmp_13 = dx02 - dx23*tmp_11 + tmp_9
	tmp_16 = dx01*tmp_12 - dy01*tmp_13
	tmp_19 = -tmp_16*tmp_3
	tmp_14 = dx23*tmp_12 - dy23*tmp_13
	tmp_18 = tmp_14*tmp_5
	tmp_17 = tmp_16*tmp_3
	tmp_15 = -tmp_14
	tmp_20 = tmp_15*tmp_5
	#logger.debug("tmp17=%s tmp_18=%s tmp_19=%s tmp_20=%s", tmp_17, tmp_18, tmp_19, tmp_20)
	if  tmp_17 >= 0 and tmp_18 >= 0 and tmp_19 >= tmp_6 and tmp_20 >= tmp_6 and tmp_6 < 0 \
	 or tmp_17 <= 0 and tmp_18 <= 0 and tmp_19 <= tmp_6 and tmp_20 <= tmp_6 and tmp_6 > 0:
		x = (dx01*tmp_15 - (tmp_9 - x0)*tmp_0)/tmp_0
		y = (dy01*tmp_15 - (tmp_10 - y0)*tmp_0)/tmp_0
		return x, y


def merged(x0, y0, x1, y1, x2, y2, x3, y3, eps=0):
	dx01 = -x0 + x1
	dy01 = -y0 + y1
	dx02 = -x0 + x2
	dy02 = -y0 + y2
	dx12 = -x1 + x2
	dy12 = -y1 + y2
	dx03 = -x0 + x3
	dy03 = -y0 + y3
	dx23 = -x2 + x3
	dy23 = -y2 + y3
	sql01 = dx01**2 + dy01**2
	sql23 = dx23**2 + dy23**2
	if not(((dx02 == 0) and (dy02 == 0) or (dx12 == 0) and (dy12 == 0) or (dx01*dy02 - dx02*dy01 == 0))\
	   and ((dx03 == 0) and (dy03 == 0) or (x1 == x3) and (y1 == y3) or (dx01*dy03 - dx03*dy01 == 0))):
		return
	tmp_0 = eps/math.sqrt(sql01)
	tmp_1 = (1/2)*tmp_0
	tmp_2 = dx01*tmp_1
	tmp_3 = eps/math.sqrt(sql23)
	tmp_4 = (1/2)*tmp_3
	tmp_5 = dx23*tmp_4
	tmp_6 = tmp_2 - tmp_5
	tmp_7 = dx02 + tmp_6
	tmp_27 = (tmp_7 <= 0)
	tmp_9 = tmp_0 + 1
	tmp_10 = dx01*tmp_9
	tmp_28 = tmp_27 or (tmp_10 >= tmp_7)
	tmp_17 = dy23*tmp_4
	tmp_16 = dy01*tmp_1
	tmp_23 = tmp_16 + tmp_17
	tmp_24 = dy03 + tmp_23
	tmp_33 = (tmp_24 <= 0)
	tmp_21 = dy01*tmp_9
	tmp_34 = tmp_33 or (tmp_21 >= tmp_24)
	tmp_8 = (tmp_7 >= 0)
	tmp_11 = tmp_8 or (tmp_10 <= tmp_7)
	tmp_18 = tmp_16 - tmp_17
	tmp_19 = dy02 + tmp_18
	tmp_20 = (tmp_19 >= 0)
	tmp_22 = tmp_20 or (tmp_21 <= tmp_19)
	tmp_31 = (tmp_19 <= 0)
	tmp_32 = tmp_31 or (tmp_21 >= tmp_19)
	tmp_12 = tmp_2 + tmp_5
	tmp_13 = dx03 + tmp_12
	tmp_14 = (tmp_13 >= 0)
	tmp_15 = tmp_14 or (tmp_10 <= tmp_13)
	tmp_25 = (tmp_24 >= 0)
	tmp_26 = tmp_25 or (tmp_21 <= tmp_24)
	tmp_29 = (tmp_13 <= 0)
	tmp_30 = tmp_29 or (tmp_10 >= tmp_13)
	if tmp_11 and tmp_15 and tmp_22 and tmp_26 and tmp_28 and tmp_30 and tmp_32 and tmp_34:
		return (x0, y0, x1, y1)
	tmp_45 = tmp_6 + x1 - x3
	tmp_51 = (tmp_45 <= 0)
	tmp_52 = tmp_51 or (tmp_10 >= tmp_45)
	tmp_48 = tmp_18 + y1 - y3
	tmp_49 = (tmp_48 >= 0)
	tmp_50 = tmp_49 or (tmp_21 <= tmp_48)
	tmp_46 = (tmp_45 >= 0)
	tmp_47 = tmp_46 or (tmp_10 <= tmp_45)
	tmp_35 = -dx12 + tmp_12
	tmp_41 = (tmp_35 <= 0)
	tmp_42 = tmp_41 or (tmp_10 >= tmp_35)
	tmp_38 = -dy12 + tmp_23
	tmp_43 = (tmp_38 <= 0)
	tmp_44 = tmp_43 or (tmp_21 >= tmp_38)
	tmp_39 = (tmp_38 >= 0)
	tmp_40 = tmp_39 or (tmp_21 <= tmp_38)
	tmp_36 = (tmp_35 >= 0)
	tmp_37 = tmp_36 or (tmp_10 <= tmp_35)
	tmp_53 = (tmp_48 <= 0)
	tmp_54 = tmp_53 or (tmp_21 >= tmp_48)
	if tmp_37 and tmp_40 and tmp_42 and tmp_44 and tmp_47 and tmp_50 and tmp_52 and tmp_54:
		return (x1, y1, x0, y0)
	tmp_55 = tmp_3 + 1
	tmp_58 = dy23*tmp_55
	tmp_59 = tmp_39 or (tmp_58 <= tmp_38)
	tmp_56 = dx23*tmp_55
	tmp_60 = tmp_41 or (tmp_56 >= tmp_35)
	tmp_68 = (tmp_58 <= 0)
	tmp_70 = (tmp_56 <= 0)
	tmp_66 = -tmp_19
	tmp_69 = (tmp_58 <= tmp_66)
	tmp_64 = -tmp_7
	tmp_65 = (tmp_56 >= tmp_64)
	tmp_62 = (tmp_56 >= 0)
	tmp_71 = (tmp_56 <= tmp_64)
	tmp_67 = (tmp_58 >= tmp_66)
	tmp_63 = (tmp_58 >= 0)
	tmp_72 = tmp_20 and tmp_27 and tmp_62 and tmp_65 and tmp_68 and tmp_69 \
	 or tmp_20 and tmp_68 and tmp_69 and tmp_70 and tmp_71 and tmp_8 \
	 or tmp_27 and tmp_31 and tmp_62 and tmp_63 and tmp_65 and tmp_67 \
	 or tmp_31 and tmp_63 and tmp_67 and tmp_70 and tmp_71 and tmp_8
	tmp_61 = tmp_43 or (tmp_58 >= tmp_38)
	tmp_57 = tmp_36 or (tmp_56 <= tmp_35)
	if tmp_57 and tmp_59 and tmp_60 and tmp_61 and tmp_72:
		return (x2, y2, x3, y3)
	tmp_74 = tmp_25 or (tmp_58 <= tmp_24)
	tmp_76 = tmp_33 or (tmp_58 >= tmp_24)
	tmp_73 = tmp_14 or (tmp_56 <= tmp_13)
	tmp_79 = -tmp_48
	tmp_80 = (tmp_58 >= tmp_79)
	tmp_77 = -tmp_45
	tmp_82 = (tmp_56 <= tmp_77)
	tmp_78 = (tmp_56 >= tmp_77)
	tmp_81 = (tmp_58 <= tmp_79)
	tmp_83 = tmp_46 and tmp_49 and tmp_68 and tmp_70 and tmp_81 and tmp_82 \
	 or tmp_46 and tmp_53 and tmp_63 and tmp_70 and tmp_80 and tmp_82 \
	 or tmp_49 and tmp_51 and tmp_62 and tmp_68 and tmp_78 and tmp_81 \
	 or tmp_51 and tmp_53 and tmp_62 and tmp_63 and tmp_78 and tmp_80
	tmp_75 = tmp_29 or (tmp_56 >= tmp_13)
	if tmp_73 and tmp_74 and tmp_75 and tmp_76 and tmp_83:
		return (x3, y3, x2, y2)
	if tmp_11 and tmp_22 and tmp_28 and tmp_32 and tmp_57 and tmp_59 and tmp_60 and tmp_61:
		return (x0, y0, x3, y3)
	if tmp_15 and tmp_26 and tmp_30 and tmp_34 and tmp_83:
		return (x0, y0, x2, y2)
	if tmp_37 and tmp_40 and tmp_42 and tmp_44 and tmp_72:
		return (x1, y1, x3, y3)
	if tmp_47 and tmp_50 and tmp_52 and tmp_54 and tmp_73 and tmp_74 and tmp_75 and tmp_76:
		return (x1, y1, x2, y2)
