# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only


def same(x1, y1, x2, y2, eps=1e-8):
	return abs(x1-x2) <= eps and abs(y1-y2) <= eps


def distance(x1, y1, x2, y2):
	return ((x1 - x2)**2 + (y1 - y2)**2) ** 0.5

