# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging

import sympy

from . import points_symbolic


logger = logging.getLogger(__name__)


def same(ref_x0, ref_y0, ref_x1, ref_y1, mes_x0, mes_y0, mes_x1, mes_y1) \
 -> bool:
	"""
	:return: whether two lines are the same
	"""

	return sympy.And(
	 collinear3(ref_x0, ref_y0, ref_x1, ref_y1, mes_x0, mes_y0),
	 collinear3(ref_x0, ref_y0, ref_x1, ref_y1, mes_x1, mes_y1),
	)


def collinear3(x0, y0, x1, y1, x2, y2):
	"""
	:return: Whether p0,p1,p2 are on the same line
	"""

	s01dx = x1 - x0
	s01dy = y1 - y0

	s02dx = x2 - x0
	s02dy = y2 - y0

	return sympy.Or(
	 # degenerate line, p0=p1, we need p2 on it
	 sympy.And(
	  sympy.Eq(x0-x2, 0), sympy.Eq(y0-y2, 0),
	  sympy.Eq(x0-x1, 0), sympy.Eq(y0-y1, 0),
	 ),
	 # if p2 is on p0
	 sympy.And(
	  sympy.Eq(x2-x0, 0), sympy.Eq(y2-y0, 0),
	 ),
	 # if p2 is on p1
	 sympy.And(
	  sympy.Eq(x2-x1, 0), sympy.Eq(y2-y1, 0),
	 ),
	 # otherwise check determinant
	 sympy.Eq(s02dx * s01dy - s02dy * s01dx, 0),
	)


def closest_point(x0, y0, x1, y1, x2, y2) \
 -> t.Tuple[t.Any,t.Any]:
	"""
	:return: closest point to p2 on line p0p1
	"""
	sex, sey = x1 - x0, y1 - y0

	# Vector SP
	spx, spy = x2 - x0, y2 - y0

	d2 = (sex * sex + sey * sey)

	# Compute the projection t of SP onto SE
	t = (spx * sex + spy * sey) / d2

	# Compute the closest point C
	cx = x0 + t * sex
	cy = y0 + t * sey
	return cx, cy


def distance_point(x0, y0, x1, y1, x2, y2) -> t.Any:
	cx, cy = closest_point(x0, y0, x1, y1, x2, y2)
	return points_symbolic.distance(x2, y2, cx, cy)
