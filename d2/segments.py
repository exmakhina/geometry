# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import typing as t


logger = logging.getLogger(__name__)


def same(ref_x0, ref_y0, ref_x1, ref_y1, mes_x0, mes_y0, mes_x1, mes_y1, eps=1e-9) \
 -> bool:
	"""
	:return: whether two segments are the same
	"""
	return (
	  abs(ref_x0 - mes_x0) < eps and
	  abs(ref_y0 - mes_y0) < eps and
	  abs(ref_x1 - mes_x1) < eps and
	  abs(ref_y1 - mes_y1) < eps
	 ) or (
	  abs(ref_x0 - mes_x1) < eps and
	  abs(ref_y0 - mes_y1) < eps and
	  abs(ref_x1 - mes_x0) < eps and
	  abs(ref_y1 - mes_y0) < eps
	 )


def length(x0, y0, x1, y1) -> float:
	"""
	:return: segment length
	"""
	return ((x0 - x1)**2 + (y0 - y1)**2)**0.5


def closest_point_on_segment(sx, sy, ex, ey, px, py, eps=1e-10):
	"""
	:return: closest point to `p` on on `se`
	:param: eps epsilon, to avoid divide by zero
	"""

	# TODO handle epsilon cases cleanly

	# Vector SE
	sex, sey = ex - sx, ey - sy

	# Vector SP
	spx, spy = px - sx, py - sy

	d2 = (sex * sex + sey * sey)

	if d2 < eps:
		return (sx, sy)

	# Compute the projection t of SP onto SE
	t = (spx * sex + spy * sey) / d2

	# Clamp the projection parameter between [0, 1] to find the closest point on the line segment SE
	t = max(0, min(1, t))

	# Compute the closest point C
	cx = sx + t * sex
	cy = sy + t * sey

	return cx, cy


def distance_point_segment(sx, sy, ex, ey, px, py):
	cx, cy = closest_point_on_segment(sx, sy, ex, ey, px, py)
	return length(px, py, cx, cy)


def elongated(sx, sy, ex, ey, sl=0, el=0) \
 -> t.Tuple[float, float, float, float]:
	"""
	Elongate a segment at its starting and/or ending points.

	:param sx, sy: Coordinates of the starting point of the segment.
	:param ex, ey: Coordinates of the ending point of the segment.
	:param sl: Length to elongate towards the starting point.
	:param el: Length to elongate towards the ending point.

	:return: A tuple containing the coordinates of the new elongated segment (nsx, nsy, nex, ney).
	"""
	# Vector from start to end
	dx, dy = ex - sx, ey - sy

	# Normalize the vector
	length = (dx**2 + dy**2)**0.5
	dx /= length
	dy /= length

	# Calculate new start and end points
	nsx, nsy = sx - sl * dx, sy - sl * dy
	nex, ney = ex + el * dx, ey + el * dy

	return nsx, nsy, nex, ney


def collapse_adjacent(sx1, sy1, ex1, ey1, sx2, sy2, ex2, ey2) \
 -> t.Optional[t.Tuple[float,float,float,float]]:
	"""
	Merge two segments if they are collinear and share exactly one end point.

	:return: The merged segment as a tuple (sx, sy, ex, ey)
	 or None if segments are not collinear or chained.
	"""

	if (ey1 - sy1) * (ex2 - sx2) != (ey2 - sy2) * (ex1 - sx1):
		# not collinear
		return

	pts = dict()

	for pt in (
	 (sx1, sy1),
	 (ex1, ey1),
	 (sx2, sy2),
	 (ex2, ey2),
	):
		pts.setdefault(pt,0)
		pts[pt] += 1

	if len(pts) == 3:
		out = []
		for pt, count in sorted(pts.items()):
			if count == 1:
				out += list(pt)
		return tuple(out)
