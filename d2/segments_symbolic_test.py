#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import logging

import sympy

from . import lines
from . import lines_symbolic
from . import points_symbolic

from .segments_symbolic import *

from exmakhina.sympy_extras.manip.base import transformed
from exmakhina.sympy_extras.manip import relational as manip_relational


logger = logging.getLogger(__name__)


def test_same_proof():
	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		dx = sympy.Symbol("dx", positive=True)
		dy = sympy.Symbol("dy", positive=True)

		r0 = sympy.Symbol("r0", positive=True)
		r1 = sympy.Symbol("r1", positive=True)

		x0 = sympy.Symbol("x0", real=True)
		y0 = sympy.Symbol("y0", real=True)

		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x1 + r1*dx*ox
		y2 = y1 + r1*dy*oy

		p0 = [x0, y0]
		p1 = [x1, y1]
		p2 = [x2, y2]

		assert same(*p0, *p0, *p0, *p0)
		assert same(*p0, *p1, *p0, *p1)
		assert same(*p0, *p1, *p1, *p0)

		assert not same(*p0, *p1, *p0, *p2)
		assert not same(*p0, *p1, *p2, *p0)

def test_elongated_cases():
	res = elongated(0, 0,  1, 1, sl=sympy.sqrt(2))
	assert same(*res, -1, -1, 1, 1)

def test_elongated_proof():
	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		dx = sympy.Symbol("dx", positive=True)
		dy = sympy.Symbol("dy", positive=True)

		r0 = sympy.Symbol("r0", positive=True)
		r1 = sympy.Symbol("r1", positive=True)

		x0 = sympy.Symbol("x0", real=True)
		y0 = sympy.Symbol("y0", real=True)

		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x1 + r1*dx*ox
		y2 = y1 + r1*dy*oy

		x3 = x0 - r1*dx*ox
		y3 = y0 - r1*dy*oy

		p0 = [x0, y0]
		p1 = [x1, y1]
		p2 = [x2, y2]
		p3 = [x3, y3]

		nl = length(*p1, *p2)

		res = elongated(*p0, *p1, el=nl)
		assert res
		assert same(*p0, *p2, *res)

		res = elongated(*p0, *p1, sl=nl)
		res = [x.simplify() for x in res]
		assert res
		assert same(*p3, *p1, *res)


def test_collinear4():
	p0 = (0,0)
	p1 = (10,10)
	p2 = (11,11)
	p3 = (30,30)

	res = collinear4(*p0, *p2, *p3, *p1, 0)
	assert res

	res = collinear4(*p0, *p1, *p3, *p2, 0)
	assert res

	p0 = (0,0)
	p1 = (10,10)
	p2 = (11.1,11)
	p3 = (30,30)

	res = collinear4(*p0, *p2, *p3, *p1, 0)
	assert not res

	res = collinear4(*p0, *p1, *p3, *p2, 0)
	assert not res


def test_closest_point_cases():
	res = closest_point(0,0, 1,1, 0,1)
	assert points_symbolic.same(*res, *([sympy.Rational(1,2)]*2)), f"Got unwanted {res}"

def v(x, y):
	return sympy.Matrix([x, y])


def test_closest_point_proof():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		p0 = v(x0, y0)

		vd = v(dx*ox, dy*oy)

		p1 = p0 + r0*vd
		p2 = p1 + r1*vd

		"""
		Prove the cases that are in front of the segment,
		considering p0p2 with p1 in the middle and p3 being
		off p1
		"""

		v2 = r2 * v(dx*ox, dy*oy)
		angle = sympy.pi/2
		v2 = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		]) * v2
		p3 = p1 + v2

		res = sympy.Matrix(closest_point(*p0, *p2, *p3))
		diff = res - p1

		diff.simplify()
		# https://github.com/sympy/sympy/issues/26378
		diff = diff.subs(sympy.Min(1, r0/(r0+r1)), r0/(r0+r1))
		diff.simplify()

		assert diff == v(0, 0)

		"""
		Prove the cases where p3 is beyond extremities,
		considering p0p1 with p2 after p1, and p3 off p2;
		the closest point is p1
		"""
		p3 = p2 + v2

		res = sympy.Matrix(closest_point(*p0, *p1, *p3))
		diff = res - p1
		diff.simplify()
		assert diff == v(0, 0)

		p3 = p0 + v2

		res = sympy.Matrix(closest_point(*p1, *p2, *p3))
		diff = res - p1
		diff.simplify()
		assert diff == v(0, 0)


def test_distance_point_segment():
	res = distance_point_segment(*[sympy.S(_) for _ in [0,0, 1,1, 0,1]])
	assert res == sympy.sqrt(2)/2


def test_symbolic_contains_point_cases():
	p0 = [0,0]
	p1 = [2,4]
	p2 = [1,2]

	res = contains_point(*p0, *p1, *p2)
	assert res

	res = contains_point(*p0, *p2, *p1)
	assert not res

	res = contains_point(*p0, *p0, *p0)
	assert res


def test_symbolic_contains_point_proof():
	"""
	Work on 4 quadrants
	"""
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x1 + r1*dx*ox
		y2 = y1 + r1*dy*oy

		p0 = [x0, y0]
		p1 = [x1, y1]
		p2 = [x2, y2]

		res = contains_point(*p0, *p0, *p0)
		assert res

		logger.debug("%s..%s ? %s", p0, p2, p1)

		res = contains_point(*p0, *p2, *p1)
		res = res.simplify()
		assert res, f"oops"

def test_symbolic_collapse_adjacent_cases():
	res = collapse_adjacent(0, 0, 1, 1, 1, 1, 2, 2)
	assert res
	assert same(*res, 0, 0, 2, 2)

	res = collapse_adjacent(0, 0, 1, 1, 1, 1, 2, 3)
	assert not res, "Not aligned"

	res = collapse_adjacent(0, 0, 1, 1, 2, 2, 3, 3)
	assert not res, "Not in contact"

	angle_den = 12
	for angle_num in range(angle_den):
		angle = 2 * sympy.pi * angle_num / angle_den

		for b in (-1,0,1):

			r = sympy.Rational(3,4)

			d0 = 5

			d03 = 10

			x0 = 0 + sympy.cos(angle) * d0
			y0 = b + sympy.sin(angle) * d0

			x1 = 0 + sympy.cos(angle) * r * d03
			y1 = b + sympy.sin(angle) * r * d03

			x2 = 0 + sympy.cos(angle) * r * d03
			y2 = b + sympy.sin(angle) * r * d03

			x3 = 0 + sympy.cos(angle) * d03
			y3 = b + sympy.sin(angle) * d03

			if 0:
				p0 = [0,0]
				p1 = [2,2]
				p2 = [1,1]
				p3 = [3,3]
			else:
				p0 = [x0, y0]
				p1 = [x1, y1]
				p2 = [x2, y2]
				p3 = [x3, y3]

			logger.debug("%s %s %s %s", p0, p1, p2, p3)

			swapped = lines.swapped

			res = collapse_adjacent(*p0, *p1, *p2, *p3)
			assert res, f"oops"
			assert same(*p0, *p3, *res), f"oops"

			res = collapse_adjacent(*p2, *p3, *p0, *p1)
			assert res, f"oops"
			assert same(*p0, *p3, *res), f"oops"

			res = collapse_adjacent(*swapped(*p0, *p1), *swapped(*p2, *p3))
			assert res, f"oops"
			assert same(*p0, *p3, *res), f"oops"

			res = collapse_adjacent(*swapped(*p2, *p3), *swapped(*p0, *p1))
			assert res, f"oops"
			assert same(*p0, *p3, *res), f"oops"

def test_symbolic_collapse_adjacent_proof():
	angle = sympy.Symbol("angle", real=True)
	b = sympy.Symbol("b", real=True)
	r0 = sympy.Symbol("r0", positive=True, nonnegative=True)
	r1 = r0 + sympy.Symbol("r1", positive=True, nonnegative=True)
	r3 = r1 + sympy.Symbol("r3", positive=True, nonnegative=True)

	x0 = 0 + sympy.cos(angle) * r0
	y0 = b + sympy.sin(angle) * r0

	x1 = 0 + sympy.cos(angle) * r1
	y1 = b + sympy.sin(angle) * r1

	x2 = 0 + sympy.cos(angle) * r1
	y2 = b + sympy.sin(angle) * r1

	x3 = 0 + sympy.cos(angle) * r3
	y3 = b + sympy.sin(angle) * r3

	p0 = [x0, y0]
	p1 = [x1, y1]
	p2 = [x2, y2]
	p3 = [x3, y3]

	logger.debug("%s %s %s %s", p0, p1, p2, p3)

	swapped = lines.swapped

	res = collapse_adjacent(*p0, *p1, *p2, *p3)
	logger.debug("Res: %s", res)

	def correct(res):
		for arg, cond in res.args:
			arg = arg.simplify()
			cond = cond.simplify()
			logger.debug("- %s", arg)
			logger.debug("  %s", cond)
			if cond == True:
				res = arg
				break
		logger.debug("Res: %s", res)
		return res

	res = correct(res)

	assert res, f"oops"
	assert same(*p0, *p3, *res), f"oops"

	res = collapse_adjacent(*p2, *p3, *p0, *p1)
	res = correct(res)
	assert res, f"oops"
	assert same(*p0, *p3, *res), f"oops"

	res = collapse_adjacent(*swapped(*p0, *p1), *swapped(*p2, *p3))
	res = correct(res)
	assert res, f"oops"
	assert same(*p0, *p3, *res), f"oops"

	res = collapse_adjacent(*swapped(*p2, *p3), *swapped(*p0, *p1))
	res = correct(res)
	assert res, f"oops"
	assert same(*p0, *p3, *res), f"oops"


def test_merged():
	res = merged(0, 0,  1, 1,  1, 1, 2, 2)
	assert same(0,0, 2,2, *res), f"oops"

	res = merged(0, 0,  1, 1,  1.1, 1.1, 2, 2)
	assert not res, f"There's a hole in the middle, merging shouldn't be possible"

	res = merged(4, 3,  5, 3,  3, 3, 4, 3)
	assert same(3,3, 5,3, *res), f"Merging horizontal"

def test_merged_proof():

	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r1", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		x1 = x0 + r0*dx*ox
		y1 = y0 + r0*dy*oy

		x2 = x1 + r1*dx*ox
		y2 = y1 + r1*dy*oy

		x3 = x2 + r2*dx*ox
		y3 = y2 + r2*dy*oy

		p0 = [x0, y0]
		p1 = [x1, y1]
		p2 = [x2, y2]
		p3 = [x3, y3]

		swapped = lines.swapped

		res = merged(*p0, *p1, *p2, *p3)
		res = res.simplify()
		assert not res, f"oops"

		res = merged(*p2, *p3, *p0, *p1)
		res = res.simplify()
		assert not res, f"oops"

		res = merged(*swapped(*p0, *p1), *swapped(*p2, *p3))
		res = res.simplify()
		assert not res, f"oops"

		res = merged(*swapped(*p2, *p3), *swapped(*p0, *p1))
		res = res.simplify()
		assert not res, f"oops"

		p1, p2 = p2, p1

		res = merged(*p0, *p1, *p2, *p3)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"

		res = merged(*p2, *p3, *p0, *p1)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"

		res = merged(*swapped(*p0, *p1), *swapped(*p2, *p3))
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"

		res = merged(*swapped(*p2, *p3), *swapped(*p0, *p1))
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"

		# same

		res = merged(*p0, *p1, *p0, *p1)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p1, *res), f"oops"

		res = merged(*p0, *p1, *p1, *p0)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p1, *res), f"oops"

		# superset
		res = merged(*p0, *p3, *p1, *p2)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"

		res = merged(*p0, *p3, *p2, *p1)
		res = res.simplify()
		assert res, f"oops"
		assert same(*p0, *p3, *res), f"oops"


def test_intersection_cases():
	res = intersection(0,0,1,1, 1,0, 0,1)
	assert points_symbolic.same(*res, *([sympy.Rational(1,2)]*2)), f"Wanted 0.5,0.5 got {res}"


	res = intersection(0,0, 1,1, 1,0, 2,1)
	assert not res, f"Wanted no intersection but got {res}"


	res = intersection(1,1,0,0, 1,0,0,1)
	assert points_symbolic.same(*res, *([sympy.Rational(1,2)]*2)), f"Wanted 0.5,0.5 got {res}"

	sympy.var("x0, y0, x1, y1, x2, y2, x3, y3", real=True)
	res = intersection(x0, y0, x1, y1, x2, y2, x3, y3).simplify()
	logger.debug("Res: %s", res)


def test_intersection_proof():
	dx = sympy.Symbol("dx", positive=True)
	dy = sympy.Symbol("dy", positive=True)

	r0 = sympy.Symbol("r0", positive=True)
	r1 = sympy.Symbol("r1", positive=True)
	r2 = sympy.Symbol("r2", positive=True)
	r3 = sympy.Symbol("r3", positive=True)

	x0 = sympy.Symbol("x0", real=True)
	y0 = sympy.Symbol("y0", real=True)
	a = sympy.Symbol("a", real=True)

	for ox, oy in (
	  (1, 1),
	  (1, -1),
	  (-1, 1),
	  (-1, -1),
	 ):
		vd = v(dx*ox, dy*oy)
		p0 = v(x0, y0)
		p1 = p0 + r0*vd

		angle = sympy.atan2(r1, 1)
		mr_atan = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		])

		"""
		Case:
			      p2
			      |
			      |
			p0----p4----p1
			      |
			      |
			      p3
		"""
		# intersection/rotation point
		p4 = p0 + (p1 - p0) *  r0 / (1 + r0)

		p2 = p4 + r1*mr_atan*vd
		p3 = p4 - r2*mr_atan*vd

		res = intersection(*p0, *p1, *p2, *p3)

		args = []
		for val, cond in res.args:
			try:
				val = v(*val) - p4
			except TypeError:
				pass
			args.append((val, cond))
		diff = res.func(*args)

		diff = transformed(diff, lambda x: manip_relational.normalize(x, sympy.expand)).expand().simplify()

		assert points_symbolic.same(*diff, *(0,0)), f"Wanted 0,0 got {diff}"

		"""
		Case:
			      p2
			      |
			      |
			p0----p4----p1
		"""
		p2 = p4 + r1*mr_atan*vd
		p3 = p4

		res = intersection(*p0, *p1, *p2, *p3)

		args = []
		for val, cond in res.args:
			try:
				val = v(*val) - p4
			except TypeError:
				pass
			args.append((val, cond))
		diff = res.func(*args)

		diff = transformed(diff, lambda x: manip_relational.normalize(x, sympy.expand)).expand().simplify()

		assert points_symbolic.same(*diff, *(0,0)), f"Wanted 0,0 got {diff}"

		res = intersection(*p2, *p3, *p0, *p1)

		args = []
		for val, cond in res.args:
			try:
				val = v(*val) - p4
			except TypeError:
				pass
			args.append((val, cond))
		diff = res.func(*args)

		diff = transformed(diff, lambda x: manip_relational.normalize(x, sympy.expand)).expand().simplify()

		assert points_symbolic.same(*diff, *(0,0)), f"Wanted 0,0 got {diff}"

		"""
		Case:
			      p3
			     /
			    /
			   p4

			 p0----p2-----p1
		"""

		p2 = p0 + a*vd

		angle = sympy.atan2(sympy.pi, 2)
		mr_90 = sympy.Matrix([
		  [ sympy.cos(angle), sympy.sin(angle)],
		  [-sympy.sin(angle), sympy.cos(angle)],
		])

		p3 = p2 + r1*mr_90*vd

		angle = sympy.atan2(r2, 1)
		mr_atan = sympy.Matrix([
			[ sympy.cos(angle), sympy.sin(angle)],
			[-sympy.sin(angle), sympy.cos(angle)],
		])

		p4 = p3 + r3*(mr_atan)*vd

		res = intersection(*p0, *p1, *p3, *p4)

		diff = manip_relational.apply(res, v)
		diff = transformed(diff, lambda x: manip_relational.normalize(x, sympy.expand)).expand().simplify()

		assert not diff
