# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t

import sympy


def same(x0, y0, x1, y1) -> bool:
	"""
	:return: whether two points are the same
	"""
	return sympy.And(
	 sympy.Eq(x0 - x1, 0),
	 sympy.Eq(y0 - y1, 0),
	)

def distance(x0, y0, x1, y1) -> t.Any:
	"""
	:return: distance between p0 and p1
	"""
	return sympy.sqrt((x0 - x1)**2 + (y0 - y1)**2)
