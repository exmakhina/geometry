#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-geometry@zougloub.eu>
# SPDX-License-Identifier: AGPL-3.0-only
# -*- coding: utf-8 vi:noet

import logging

from .lines_symbolic_slack import *


logger = logging.getLogger(__name__)


def notest_collinear3_slack():
	for (dx,dy) in ((0,1),(1,0),(0,-1),(-1,0), (1,1), (1,-1), (-1,1), (-1,-1)):
		for (x0, y0) in ((0,0),):
			for mul in (1,2,-1,0):
				x1 = x0 + dx
				y1 = y0 + dx
				x2 = x0 + dx*mul
				y2 = x0 + dx*mul

				def situation():
					return f"in line({x0},{y0}-{x1},{y1}) & pt({x2}, {y2})"

				res = collinear3_slack(x0, y0, x1, y1, x2, y2)
				assert res

				# Put a point in a direction we aren't implementing
				x2 += 0.1
				y2 += 0.2
				res = collinear3_slack(x0, y0, x1, y1, x2, y2, 0.5)
				assert res, "Should be collinear {situation()}"

				# Put it further
				x2 += 0.3
				y2 += 0.6
				res = collinear3_slack(x0, y0, x1, y1, x2, y2, 0.5)
				assert not res, f"Shouldn't be collinear {res=} {situation()}"

